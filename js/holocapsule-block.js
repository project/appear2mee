(function ($, Drupal) {
  Drupal.behaviors.ToMEEHoloCapsuleBlock = {
    attach: function (context, settings) {
      if (context !== document && !$(context).is('form'))
        return;
      $.getJSON("/2mee-holocapsule/list", function(data) {
        if ('error' in data) {
          if (!$('.messages--error', context).length)
            $('.form-submit:first', context).click();
        }
        else {
          $('select[name*=holocapsule_message]', context).empty();
          for (var option of data)
            $('select[name*=holocapsule_message]', context)
              .append("<option value='" + option.title + "'>" + option.title + "</option>");
          $('select[name*=holocapsule_message]', context).attr("disabled", false);
          if (settings.holocapsule_message_selected)
            $('select[name*=holocapsule_message] option[value="' + settings.holocapsule_message_selected + '"]', context).attr('selected', true);
        }
      });
    }
  };
})(jQuery, Drupal);
