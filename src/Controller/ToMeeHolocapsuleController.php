<?php

namespace Drupal\tomee\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for tomee routes.
 */
class ToMeeHolocapsuleController extends ControllerBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'tomee.settings';
  const URL = 'http://server-v1.2mee.com/slot/list';
  const APP_ID_LENGTH = 36;
  /**
   * Returns HoloCapsule Messages.
   */
  public function list($http_responce = TRUE) {
    $config = \Drupal::config(static::SETTINGS);
    $client = \Drupal::httpClient();

    try {
      $response = $client->get(static::URL . '?appId=' . mb_substr($config->get('web_id'), 0, static::APP_ID_LENGTH) . '&active=true', [
        'headers' => [
          'Content-Type'  => 'application/json',
          'Authorization' => 'Bearer ' . $config->get('api_key'),
        ],
      ]);
      $data = (string) $response->getBody();
      if (empty($data)) {
        return [];
      }

      $data = Json::decode($data);
      $available_holocapsules = [];
      if ($data['totalElements']) {
        foreach ($data['content'] as $holocapsule) {
          array_push($available_holocapsules, [
              'id' => $holocapsule['id'],
              'title' => $holocapsule['title'],
            ]
          );
        }
      }
      return $http_responce ? new JsonResponse($available_holocapsules): $available_holocapsules;
    }
    catch (\Exception $e) {
      $settings_link = \Drupal\Core\Link::fromTextAndUrl(t('Go to 2Mee Settings'), new \Drupal\Core\Url('tomee.settings_form'))->toRenderable();
      $settings_link = \Drupal::service('renderer')->render($settings_link);
      if ($e->hasResponse()) {
        $data = $e->getResponse()->getBody()->getContents();
        $data = Json::decode($data);
        if (isset($data['apiError'])) {
          $error = [
            'error' => t('2Mee: ' . $data['apiError']['status'] . ' (' . $data['apiError']['statusCode'] . '). ' . $data['apiError']['message']) . '. ' . $settings_link
          ];
          \Drupal::messenger()->addError(t($error['error'], ['%destination' => $settings_link]));
          return $http_responce ? new JsonResponse($error) : $error;
        }
      }
      \Drupal::messenger()->addError(t('2Mee: Some connection problem') . '. ' . $settings_link);
      return FALSE;
    }
  }
}
