<?php

namespace Drupal\tomee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure tomee settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'tomee.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tomee_holocapsule_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::SETTINGS,];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['web_id'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Web ID'),
      '#default_value' => $config->get('web_id'),
      '#rows' => 2,
    ];

    $form['api_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#rows' => 2,
    ];

    $form['css_default_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => array_combine($config->get('css_positions'), $config->get('css_positions')),
      '#default_value' => $config->get('css_default_position'),
    ];

    $form['include_on_any_page'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Include on any page'),
      '#default_value' => $config->get('include_on_any_page'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('web_id', $form_state->getValue('web_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('css_default_position', $form_state->getValue('css_default_position'))
      ->set('include_on_any_page', $form_state->getValue('include_on_any_page'))
      ->save();

    $query = \Drupal::entityQuery('block');
    $query->condition('plugin', ['tomee_holocapsule_block', 'tomee_topic_block']);
    $bids = $query->execute();
    $tags = [];
    $tag_prefix = 'config:block.block.';
    foreach ($bids as $bid) {
      array_push($tags, $tag_prefix . $bid);
    }

    \Drupal\Core\Cache\Cache::invalidateTags($tags);

    parent::submitForm($form, $form_state);
  }
}
