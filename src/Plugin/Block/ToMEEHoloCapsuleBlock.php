<?php

namespace Drupal\tomee\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "tomee_holocapsule_block",
 *   admin_label = @Translation("2MEE HoloCapsule Message"),
 *   category = @Translation("2MEE")
 * )
 */
class ToMEEHoloCapsuleBlock extends BlockBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'tomee.settings';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'message_type' => 'in-place-message',
      'holocapsule_message' => '',
      'event_message' => [
        'html_tag' => '',
        'html_tag_text' => 'My button',
        'event_message_url' => '',
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['message_type'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Message types'),
      '#options'       => [
        'in-place-message' => t('In Place Message'),
        'event-message' => t('Event Message'),
      ],
      '#default_value' => $this->configuration['message_type'],
      '#required' => TRUE,
    ];

    $form['holocapsule_message'] = [
      '#type' => 'select',
      '#title' => $this->t('HoloCapsule Messages'),
      '#options' => [$this->t('Trying to retrieve available HoloCapsule messages...')],
      '#disabled' => TRUE,
    ];

    if (!isset($form_state->getUserInput()['settings']['holocapsule_message'])) {
      $form['holocapsule_message']['#disabled'] = TRUE;
      $form['#attached'] = [
        'library' => [
          'tomee/tomee_holocapsule_block',
        ],
        'drupalSettings' => [
          'holocapsule_message_selected' => $this->configuration['holocapsule_message'],
        ],
      ];
    }

    $form['event_message'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Event Message Settings'),
      'html_tag' => [
        '#type' => 'select',
        '#options' => [
          'button' => $this->t('Button'),
        ],
        '#default_value' => $this->configuration['event_message']['html_tag'],
      ],
      'html_tag_text' => [
        '#type' => 'textfield',
        '#title' => $this->t('Text'),
        '#default_value' => $this->configuration['event_message']['html_tag_text'],
        '#required' => TRUE,
      ],
      'event_message_url' => [
        '#type' => 'url',
        '#title' => $this->t('URL (optional'),
        '#default_value' => $this->configuration['event_message']['event_message_url'],
        '#attributes' => ['placeholder' => 'http://example.com'],
      ],
      '#states' => [
        'visible' => [
          'input[name="settings[message_type]"]' => ['value' => 'event-message'],
        ],
      ],
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!isset($form_state->getUserInput()['settings']['holocapsule_message'])) {
      $form_state->setErrorByName('holocapsule_message');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $formState) {
    $this->configuration['message_type'] = $formState->getValue('message_type');
    $this->configuration['holocapsule_message'] = $formState->getUserInput()['settings']['holocapsule_message'];
    if ($this->configuration['message_type'] == 'event-message') {
      $this->configuration['event_message'] = $formState->getValue('event_message');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config(static::SETTINGS);
    if ($this->configuration['message_type'] == 'event-message') {
      if ($this->configuration['event_message']['html_tag'] == 'button') {
        $build['content'] = [
          '#theme' => empty($this->configuration['event_message']['event_message_url']) ? 'tomee_event_button': 'tomee_event_with_url_button',
          'holocapsule_text' => ['#markup' => $this->configuration['event_message']['html_tag_text']],
          'holocapsule_message' => ['#markup' => $this->configuration['holocapsule_message']],
        ];

        if (!empty($this->configuration['event_message']['event_message_url'])) {
          $build['content']['event_message_url'] = ['#markup' => $this->configuration['event_message']['event_message_url']];
        }
      }
    }
    else {
      $build['content'] = [
        '#theme' => 'tomee_in_place',
        'holocapsule_message' => ['#markup' => $this->configuration['holocapsule_message']],
      ];
    }
    $build['content']['#attached'] = [
      'library'        => [
        'tomee/appear2mee',
      ],
      'drupalSettings' => [
        'web_id'       => $config->get('web_id'),
        'css_position' => $config->get('css_default_position'),
      ],
    ];
    return $build;
  }
}
