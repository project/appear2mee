<?php

namespace Drupal\tomee\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Provides an example block.
 *
 * @Block(
 *   id = "tomee_topic_block",
 *   admin_label = @Translation("2MEE Topic Message"),
 *   category = @Translation("2MEE")
 * )
 */
class ToMEETopicBlock extends BlockBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'tomee.settings';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'topics' => '[current-page:title]',
      'tokens' => ['node', 'user'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['topics'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Topics'),
      '#default_value' => $this->configuration['topics'],
      '#size' => 65,
      '#maxlength' => 1280,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => $this->configuration['tokens'],
      '#description' => $this->t('Please separate values by comma. Tokens can be used.'),
    ];

    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => $this->configuration['tokens'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['topics'] =  \Drupal\Component\Utility\Html::escape($form_state->getValue('topics'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config(static::SETTINGS);
    $token_service = \Drupal::token();
    $build['content']['#attached'] = [
      'library' => [
        'tomee/appear2mee',
        'tomee/tomee_topic',
      ],
      'drupalSettings' => [
        'web_id'       => $config->get('web_id'),
        'css_position' => $config->get('css_default_position'),
        'topic' => array_map('trim', explode(',', $token_service->replace($this->configuration['topics'], $this->configuration['tokens']))),
      ],
    ];
    return $build;
  }
}
