CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Recommended modules
 * Configuration
 * Upgrading

INTRODUCTION
------------

The 2MEE module provides a simple integration between 2mee’s Instant Human Hologram Messaging service(https://go.2mee.com/) and Drupal.

The primary goal of this module is to integrate a 2mee HoloCapsule or a 2mee Topic Message on your websites pages through the Drupal block system.  
You can find more information about 2mee’s Instant Human Hologram Messaging at https://docs.2mee.com/

INSTALLATION
------------

The installation of this module is similar to other Drupal modules.
 1. Create an account or request a demo from 2mee(https://go.2mee.com/).
 
 2. Copy/upload the 2MEE module to the modules directory of your Drupal
   installation.

 3. Enable the '2MEE' module in 'Extend'. (/admin/modules)

 4. Set up user permissions. (/admin/people/permissions#module-webform)

 5. Go to the 2MEE Settings (/admin/config/system/2mee page). There is an API Key & Web ID that can be retrieved from
    2mee(https://go.2mee.com/) user settings:
    * There is a related tab on user profile page where a new API key can be generated (https://go.2mee.com/#!/user-profile/api-details).
      Copy the API key and add this to Drupal's 2MEE Settings.
    * Web ID is related to website entity. The website can be added on the All Applications (https://go.2mee.com/#!/applications) page of the 2mee platform before the next step. In the entity setting there is a Web Push link. By clicking the code that is used to run 2mee human hologram messaging on web a page appears. initWithID settings value can be copied (without quotes) to Web ID field of Drupal's 2MEE Settings.
    
RECOMMENDED MODULES
-------------------

 * (Optional) [Custom Context Provider](https://github.com/LukasKlement/custom_context_provider) needs to be installed for taxonomy
   terms, this will provide context support for using tokens. Required only for Drupal 9.x (or 8.x) < 9.2, the newest versions
   of Drupal have built-in support.
 * (Optional) [Entity Clone](https://www.drupal.org/project/entity_clone) will allow you to add the possibility to clone blocks when required.

CONFIGURATION
-------------
 * Read 2mee docs(https://docs.2mee.com/) to get base understanding of how things works.
   Then try to create some HoloCapsule and Topic Messages that can be used on the site.
 * Build a new 2MEE block (/admin/structure/block) by clicking on the Place block (region doesn't matter). There are two types of blocks - "2MEE HoloCapsule Message", "2MEE Topic Message".
   - When "HoloCapsule Message" is selected, a form with configuration appears. You are then able to use "HoloCapsule Messages" that are populated with an already recorded message from the 2mee platform. You can place the block on several pages, so sometimes it's more flexible to use the tokens context depending on which page is used. Multiple tokens can also be chosen at once. When one of the generated values of the selected tokens corresponds to a recorded message the module chooses the first one to be used on the page. Also user is able to select message type - "In Place Message" or "Event Message". First one appears on the page once it is loaded. Second - adds a clickable button on the page, which loads HoloCapsule Message. There are theme templates that can be overided using the standard Drupal method. See https://docs.2mee.com/documentation/web-sdk#inline-messages or https://docs.2mee.com/documentation/web-sdk#event-messages for more information about Inline & Event Messages.
   - When "2MEE Topic Message" is selected, a form with configuration appears. Topics can be chosen here by placing tokens or adding text. All topics will be connected (or created if not exists) to corresponding topic channels and messages will be received once the topic is live. Receive option can be configured on the configuration form.
     More information regarding the functionality of Topics can be found at (https://docs.2mee.com/documentation/web-sdk).

UPGRADING
---------

 * All existing configuration and data was maintained and updated
   through the beta and rc release cycles.
   **APIs have changed** during these release cycles.